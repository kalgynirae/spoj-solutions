#!/usr/bin/python3
from collections import defaultdict
import sys

nodes, edges = map(int, input().split())
if edges != nodes - 1:
    print("NO")
    sys.exit(0)

graph = defaultdict(list)
for _ in range(edges):
    a, b = map(int, input().split())
    graph[a].append(b)
    graph[b].append(a)

# bfs
visited = set()
queue = [1]
while len(queue) > 0:
    i = queue.pop(0)
    visited.add(i)
    for q in graph[i]:
        if q not in visited:
            queue.append(q)
if len(visited) == nodes:
    print("YES")
else:
    print("NO")
