#!/bin/python3
from itertools import chain, product
from math import sqrt
import sys
from time import sleep

class Invalid(Exception): pass

def debug(possibles):
    if '--debug' in sys.argv:
        print(format(possibles, '{:<8}'))
        sleep(0.01)

def nope(possibles, spot, value):
    """Remove value from the possibilities for spot"""
    if value not in possibles[spot]:
        return
    possibles[spot] = possibles[spot].replace(value, '')
    debug(possibles)
    if len(possibles[spot]) == 0:
        raise Invalid("No possibilities left for spot %s" % (spot,))
    elif len(possibles[spot]) == 1:
        yup(possibles, spot, possibles[spot])

def yup(possibles, spot, value):
    """Choose value for spot, removing all other possibilities"""
    other_possibles = possibles[spot].replace(value, '')
    possibles[spot] = value
    debug(possibles)
    for neighbor in neighbors[spot] - {spot}:
        nope(possibles, neighbor, value)
    for possible in other_possibles:
        for unit in units[spot]:
            spots = [spot for spot in unit if possible in possibles[spot]]
            if len(spots) == 1 and len(possibles[spots[0]]) > 1:
                yup(possibles, spots[0], possible)

def solve(possibles):
    if all(len(values) == 1 for values in possibles.values()):
        # Behold, it is solved!
        return possibles
    # Find the undecided spot with the fewest choices
    _, spot = min((len(values), spot) for spot, values in possibles.items()
                  if len(values) > 1)
    for value in possibles[spot]:
        # Try to solve the problem with value picked for spot
        pv = possibles.copy()
        try:
            yup(pv, spot, value)
            possibles = solve(pv)
        except Invalid:
            continue
        else:
            return possibles
    else:
        raise Invalid("No more values to try")

def format(possibles, format='{}'):
    return '\n'.join(''.join(format.format(item[1]) for item in items)
                     for items in zip(*[iter(sorted(possibles.items()))] *
                                      size**2))

# Functions to get the spots in the row, column, and box for a particular spot
# Note that these return sets!
row = lambda r, _: {(r, c) for c in choices}
col = lambda _, c: {(r, c) for r in choices}
def box(r, c):
    row_start = (choices.find(r) // size) * size
    col_start = (choices.find(c) // size) * size
    return {(r, c) for r in choices[row_start:row_start+size]
                   for c in choices[col_start:col_start+size]}

# Specify the choices for a spot in the Sudoku puzzle. Each choice must be a
# single character.
choices = '123456789'
input_placeholder_character = '0'

size = int(sqrt(len(choices)))
if size**2 != len(choices): raise ValueError("choices must be have a square "
                                             "length")

spots = [(r, c) for r in choices for c in choices]
units = {spot: [row(*spot), col(*spot), box(*spot)] for spot in spots}
neighbors = {spot: r | c | b for spot, (r, c, b) in units.items()}

if __name__ == '__main__':
    cases = int(input())
    for _ in range(cases):
        possibles = {spot: choices for spot in spots}
        debug(possibles)
        for spot, value in zip(spots, chain(*[input().split() for _ in range(size**2)])):
            if value != input_placeholder_character:
                yup(possibles, spot, value)
        try:
            solved = solve(possibles)
        except Invalid:
            print("This board has no solution.")
        else:
            print(format(solved))
