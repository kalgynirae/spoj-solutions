def factorial(n):
    return 1 if n < 2 else n * factorial(n - 1)

cases = int(raw_input())
for _ in range(cases):
    print(factorial(int(raw_input())))
