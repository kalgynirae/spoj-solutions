#!/usr/bin/python2
import sys

def main():
    for line in sys.stdin:
        if line.startswith('*'):
            break
        words = line.lower().split()
        letter = words[0][0]
        print "Y" if all(w[0] == letter for w in words[1:]) else "N"

main()
