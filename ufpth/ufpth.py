case = 0
while True:
    case += 1
    n, m = (int(x) for x in raw_input().split())
    if n == 0:
        break
    d = {}
    for _ in xrange(m):
        a, b = (int(x) for x in raw_input().split())
        l = d.get(a, [a]) + d.get(b, [b])
        for v in set(l):
            d[v] = l
    # Add nodes with no edges
    for i in xrange(1, n + 1):
        if i not in d:
            d[i] = [i]
    trees = sum(len(pg) == len(set(pg)) for pg in set(map(tuple, d.itervalues())))
    if trees == 0:
        print "Case %d: No trees." % case
    elif trees == 1:
        print "Case %d: There is one tree." % case
    else:
        print "Case %d: A forest of %d trees." % (case, trees)
