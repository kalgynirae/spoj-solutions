messages = {0: "Airborne wins.", 1: "Pagfloyd wins."}
case_count = int(input())
for _ in range(case_count):
    n, starting_player_number = [int(s) for s in input().split()]
    print(messages[starting_player_number])
