from collections import Counter
from itertools import chain, islice, izip
from math import factorial as f
from operator import mul

def perms(l):
    return sum(f(l - g) / (f(g) * f(l - g - g)) for g in xrange(l / 2 + 1))

def count_decodings(s):
    #print(s)
    lengths = Counter()
    current_segment_length = 0
    for previous_c, c in izip(chain('1', s), s):
        if c == '0':
            lengths[current_segment_length - 1] += 1
            lengths[1] += 1
            current_segment_length = 0
        elif previous_c in '03456789' or (previous_c == '2' and c in '789'):
            lengths[current_segment_length] += 1
            current_segment_length = 1
        else:
            current_segment_length += 1
    else:
        lengths[current_segment_length] += 1
        current_segment_length = 0
    #print(lengths)
    return reduce(mul, chain(*[[perms(length)] * count
                             for length, count in lengths.items()]))

while True:
    string = raw_input()
    if string == '0':
        break
    print(count_decodings(string))
