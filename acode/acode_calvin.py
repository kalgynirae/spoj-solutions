#!/usr/bin/python3

def acode(inp):
    score = 0
    last = 1
    prev = 1
    for i in range(len(inp) - 2, -1, -1):
        if (inp[i] == 1 or (inp[i] == 2 and inp[i+1] < 7)) and inp[i+1] > 0:
            if i + 2 < len(inp) and inp[i+2] > 0 or i + 2 >= len(inp):
                score = last + prev
                prev = last
                last = score
            else:
                score = 1
        elif inp[i] > 0 and inp[i+1] > 0:
            if i+2 < len(inp) and inp[i+2] > 0 or i+2 >= len(inp) or inp[i] < 3:
                prev = last
                score = last
        elif inp[i] > 0 and inp[i] < 3:
            score = last
    return score

if __name__ == "__main__":
    while True:
        num = input()
        if num == "0":
            break
        print(acode([int(d) for d in num]))

