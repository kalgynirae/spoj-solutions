#!/usr/bin/python2
from itertools import permutations
from string import ascii_lowercase
import sys

def palindromic(s):
    return all(a == b for a, b in zip(s, s[-1:len(s)//2-1:-1]))

for line in sys.stdin:
    letters = [c for c in line.lower() if c in ascii_lowercase]
    print sum(palindromic(p) for p in permutations(letters))
