i = 0
while True:
    L, P, V = (int(x) for x in raw_input().split())
    i += 1
    if L == 0:
        break
    answer = V / P * L + min((V % P, L))
    print 'Case %d: %d' % (i, answer)
