n = int(input())
cases = (int(input()) for i in range(n))
for sides in cases:
    answer = sides * sum(1/k for k in range(1, sides+1))
    print("%.2f" % answer)
