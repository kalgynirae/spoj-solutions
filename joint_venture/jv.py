from bisect import bisect_left
import itertools
from sys import stdin

try:
    while True:
        opening_width = int(next(stdin)) * 10000000
        piece_count = int(next(stdin))
        lengths = sorted(map(int, itertools.islice(stdin, piece_count)))

        danger = True
        for i, l1 in enumerate(lengths):
            remaining_width = opening_width - l1
            if remaining_width < l1:
                break
            j = bisect_left(lengths, remaining_width)
            if j < piece_count and lengths[j] == remaining_width and j != i:
                print "yes %d %d" % (l1, remaining_width)
                danger = False
                break

        if danger: print "danger"

except StopIteration:
    pass
