while True:
    a, d = map(int, input().split())
    if a == 0 and d == 0:
        break
    a = [(int(s), True) for s in input().split()]
    d = [(int(s), False) for s in input().split()]
    distances = sorted(a + d)
    if distances[0][1] or distances[1][1]:
        print('Y')
    else:
        print('N')
