def palindromic(s):
    return all(a == b for a, b in zip(s, s[-1:len(s)//2-1:-1]))

def next_palindrome(s):
    if s == '9': return '11'
    stop = len(s) // 2
    middle = s[stop] if len(s) % 2 == 1 else ''
    palindrome = s[:stop] + middle + s[:stop][::-1]
    if palindrome > s:
        return palindrome
    else:
        # Raise the middle value
        half = palindrome[:stop + (1 if middle else 0)]
        raised_half = str(int(half) + 1)
        return raised_half + raised_half[
                -1 - (1 if middle else 0) -
                (1 if len(raised_half) > len(half) else 0)
                ::-1]

cases = int(raw_input())
for _ in range(cases):
    s = raw_input()
    nsp = next_palindrome(s)
    print(nsp)
