from itertools import islice

def palindromic(s):
    return all(a == b for a, b in zip(s, s[-1:len(s)//2-1:-1]))

def next_smallest_palindrome(s):
    if len(s) == 1:
        if s == '9':
            return '11'
        return str(int(s) + 1)
    else:
        a = ai = len(s) // 2 - 1
        b = bi = a + 1 + (len(s) % 2)
        while a >= 0:
            if s[a] != s[b]:
                break
            a -= 1
            b += 1
        else:
            # it's a palindrome already
            # is there a middle digit that's not nine?
            
            # work outward till we find a pair of non-nines
            a, b = ai, bi
            while a >= 0:
                if s[a] != '9':
                    break
                a -= 1
                b += 1
            else:
                # it's all nines
                return '1' + '0' * (len(s) - 1) + '1'
            # we found a pair of non-nines; increment them and make inner
            # values all zero
        # we know a and b are inequal
        if s[a] > s[b]:
            # raise b to equal a
            return s[:b] + s[a] + s[b+1:]
        else:
            # lower b to equal a and also raise the innermost pair
            if a == ai:
                # this is the innermost pair
                # raise a by 1 and make b equal to a
                new = str(int(s[a])+1)
                return s[:a] + new + s[a+1:b] + new + s[b+1:]
    return 'nope'

cases = int(raw_input())
with open('expected_output') as f:
    lines = f.readlines()
correct = (l.strip() for l in lines)
for _ in range(cases):
    s = raw_input()
    nsp = next_smallest_palindrome(s)
    print(("wrong: " if nsp != next(correct) else "") +
          "{}->{}".format(s, nsp))
