def increment(s, index):
    s[index] += 1
    for i in range(index, 0, -1):
        if s[i] == 10:
            s[i] = 0
            s[i-1] += 1
    if s[0] == 10:
        s[0] = 0
        s.insert(0, 1)

def next_palindrome(s):
    if s == '9': return '11'
    first_half_len = len(s) // 2 + (len(s) % 2)
    second_half_len = len(s) - first_half_len
    palindrome = s[:first_half_len] + s[:second_half_len][::-1]
    if palindrome > s:
        return palindrome
    # Raise the middle value
    increment(palindrome, first_half_len - 1)
    # Palindromize it again
    first_half_len = len(palindrome) // 2 + (len(palindrome) % 2)
    second_half_len = len(palindrome) - first_half_len
    return palindrome[:first_half_len] + palindrome[:second_half_len][::-1]

cases = int(raw_input())
for _ in range(cases):
    s = map(int, raw_input())
    np = next_palindrome(s)
    print(''.join(map(str, np)))
