#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* increment(char* s, int index, int* length) {
    s[index] += 1;
    int i;
    for (i = index; i > 0; i--) {
        if (s[i] == 10) {
            s[i] = 0;
            s[i - 1] += 1;
        }
    }
    if (s[0] == 10) {
        s[0] = 0;
        // Allocate a new array
        *length += 1;
        char* q = calloc(*length, sizeof(char));
        q[0] = 1;
        memcpy(q + 1, s, *length - 1);
        return q;
    }
    return s;
}

char* next_palindrome(char* s, int* length) {
    if (s[0] == 9 && *length == 1) {
        char* e = calloc(3, sizeof(char));
        e[0] = 1;
        e[1] = 1;
        *length = 2;
        return e;
    }
    int first_half_len = *length / 2 + (*length % 2);
    int second_half_len = *length - first_half_len;
    char* p = calloc(*length, sizeof(char));
    memcpy(p, s, first_half_len);
    int i;
    for (i = 0; i < second_half_len; i++) {
        p[*length - 1 - i] = s[i];
    }
    bool larger = false;
    for (i = 0; i < *length; i++) {
        if (p[i] < s[i]) break;
        if (p[i] > s[i]) {
            larger = true;
            break;
        }
    }
    if (larger) return p;
    // Raise the middle value
    char* q = increment(p, first_half_len - 1, length);
    // Palindromize it again
    first_half_len = *length / 2 + (*length % 2);
    second_half_len = *length - first_half_len;
    char* r = calloc(*length, sizeof(char));
    memcpy(r, q, first_half_len);
    for (i = 0; i < second_half_len; i++) {
        r[*length - 1 - i] = q[i];
    }
    return r;
}

int main() {
    int cases;
    scanf("%d", &cases);
    int i;
    for (i = 0; i < cases; i++) {
        char s[1000001];
        scanf("%s", s);
        int length = strlen(s);
        char* p = s;
        while (*p) {
            // Reduce the characters to their integer values
            *p -= '0';
            p++;
        }
        char* q = next_palindrome(s, &length);
        char* r = q;
        int j;
        for (j = 0; j < length; j++) {
            *r += '0';
            r++;
        }
        printf("%s\n", q);
    }
    return 0;
}
