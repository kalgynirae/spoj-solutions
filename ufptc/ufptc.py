import itertools

def primes():
    yield 2
    D = {}
    for q in itertools.count(3, 2):
        p = D.pop(q, None)
        if p is None:
            yield q
            D[q * q] = 2 * q
        else:
            x = p + q
            while x in D:
                x += p
            D[x] = p

cases = []
while True:
    i = raw_input()
    if i == '0':
        break
    cases.append(tuple(int(x) for x in i.split()))

for case_number, case in enumerate(cases, 1):
    a, b, L, U = case
    count = 0
    p = primes()
    j = next(p)
    for n in xrange(L, U + 1):
        t = a * n + b
        while j < t:
            j = next(p)
        if j == t:
            count += 1
    print("Case %d: %d" % (case_number, count))
