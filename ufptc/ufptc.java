import java.util.*;

class ufptc
{
	public static void main(String args[])
	{
		Scanner s = new Scanner(System.in);
		long case_number = 0;
		while (true)
		{
			case_number++;
			long a = s.nextLong();
			if (a == 0)
				break;
			long b = s.nextLong();
			long L = s.nextLong();
			long U = s.nextLong();
			long count = 0;
			HashMap<Long, HashSet<Long>> D = new HashMap<Long, HashSet<Long>>();
			long j = 1;
			for (long n = L; n < U + 1; n++)
			{
				long t = a * n + b;
				while (j < t)
				{
					while (true)
					{
						j += 1;
						if (!D.containsKey(j))
						{
							HashSet<Long> z = new HashSet<Long>();
							z.add(j);
							D.put(j * j, z);
							break;
						}
						Iterator<Long> iter = D.get(j).iterator();
						while (iter.hasNext())
						{
							long p = iter.next();
							if (D.containsKey(p + j))
								D.get(p + j).add(p);
							else
							{
								HashSet<Long> z = new HashSet<Long>();
								z.add(p);
								D.put(p + j, z);
							}
						}
						D.remove(j);
					}
				}
				if (j == t) count++;
			}
			System.out.println("Case " + case_number + ": " + count);
		}
	}
}
