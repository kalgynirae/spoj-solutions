while True:
    try:
        n = int(input())
    except EOFError:
        break
    print(2 * n - 2 if n > 1 else 1 if n == 1 else 0)
