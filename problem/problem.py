t = [('B8', 2), ('ADOPQRabdegopq690', 1)]
d = {c: n for cs, n in t for c in cs}
count = int(input())
for _ in range(count):
    print(sum(d.get(c, 0) for c in input()))
