t = [('B8', 2), ('ADOPQRabdegopq690', 1)]
d = {c: n for cs, n in t for c in cs}
count = int(raw_input())
for _ in xrange(count):
    print(sum(d.get(c, 0) for c in raw_input()))
