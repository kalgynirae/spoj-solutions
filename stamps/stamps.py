import heapq

scenario_count = int(raw_input())
for i in xrange(1, scenario_count + 1):
    needed_stamp_count, _ = [int(s) for s in raw_input().split()]
    h = []
    for stamps in [int(s) for s in raw_input().split()]:
        heapq.heappush(h, -stamps)
    total = 0
    n = 0
    while total < needed_stamp_count:
        try:
            total += -heapq.heappop(h)
        except IndexError:
            n = -1
            break
        else:
            n += 1
    print "Scenario #%d:\n%s" % (i, n if n > -1 else 'impossible')
    if i != scenario_count:
        print
