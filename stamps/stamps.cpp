#include <iostream>
#include <queue>
using namespace std;

int main() {
    int scenario_count;
    cin >> scenario_count;
    for (int i = 1; i < scenario_count + 1; i++) {
        int friend_count, needed_stamp_count;
        cin >> needed_stamp_count;
        cin >> friend_count;

        priority_queue<int> pq;
        int loneable_stamp_count;
        for (int j = 0; j < friend_count; j++) {
            cin >> loneable_stamp_count;
            pq.push(loneable_stamp_count);
        }
        int total_stamp_count = 0;
        int borrowed_friends = 0;
        while (total_stamp_count < needed_stamp_count) {
            if (!pq.empty()) {
                total_stamp_count += pq.top();
                pq.pop();
                borrowed_friends ++;
            }
            else {
                borrowed_friends = -1;
                break;
            }
        }
        cout << "Scenario #" << i << ":" << endl;
        if (borrowed_friends >= 0) {
            cout << borrowed_friends << endl;
        }
        else {
            cout << "impossible" << endl;
        }
        if (i != scenario_count) {
            cout << endl;
        }
    }
    return 0;
}
