#!/usr/bin/python2
import re

def ralign(string, width):
    return '{{:>{}}}'.format(width).format(string).rstrip()

def add(n1, n2):
    sum = int(n1) + int(n2)
    width = max(len(n1), len(n2) + 1, len(str(sum)))
    lines = [n1, '+' + n2, '-' * width, sum]
    return '\n'.join(ralign(x, width) for x in lines)

def subtract(n1, n2):
    difference = int(n1) - int(n2)
    width = max(len(n1), len(n2) + 1)
    hyphens_width = max(len(n2) + 1, len(str(difference)))
    lines = [n1, '-' + n2, '-' * hyphens_width, difference]
    return '\n'.join(ralign(x, width) for x in lines)

def multiply(n1, n2):
    n1i = int(n1)
    product = n1i * int(n2)
    width = max(len(n1), len(n2) + 1, len(str(product)))
    partial_products = [str(n1i * int(digit)) + ' ' * offset
                        for offset, digit in enumerate(n2[::-1])]
    hyphens_width = max(len(n2) + 1, len(partial_products[0]))
    lines = [n1, '*' + n2, '-' * hyphens_width]
    if len(partial_products) > 1:
        lines.extend(partial_products)
        lines.append('-' * max(len(partial_products[-1]), len(str(product))))
    lines.append(product)
    return '\n'.join(ralign(x, width) for x in lines)

if __name__ == '__main__':
    cases = int(raw_input())
    operations = {'+': add, '-': subtract, '*': multiply}
    pattern = re.compile(r'([+\-*])')
    for _ in range(cases):
        n1, op, n2 = pattern.split(raw_input().strip())
        print operations[op](n1, n2)
        print
