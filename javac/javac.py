#!/usr/bin/python2
from string import ascii_lowercase, ascii_uppercase
import sys

def java_to_c(identifier):
    return ''.join(c if c in ascii_lowercase else '_' + c.lower()
                   for c in identifier)

def c_to_java(identifier):
    capitals = [i+1 for i, c in enumerate(identifier) if c == '_']
    return ''.join(c.upper() if i in capitals else c
                   for i, c in enumerate(identifier) if c != '_')

def translate(identifier):
    if '_' in identifier:
        # Check whether everything is lowercase
        if identifier.lower() == identifier:
        #if all(c in ascii_lowercase for c in identifier if c != '_'):
            # Check whether underscores are properly placed
            if all(len(part) > 0 for part in identifier.split('_')):
                return c_to_java(identifier)
    else:
        if identifier[0] in ascii_lowercase:
            return java_to_c(identifier)
    return 'Error!'

for line in sys.stdin:
    print translate(line.strip())
