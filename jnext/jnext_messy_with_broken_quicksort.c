#include <stdio.h>
#include <stdlib.h>

void swap(int* x, int* y) {
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

void quicksort(int* list, int start, int end) {
    if (start < end) {
        int pivot_i = (start + end) / 2;
        swap(&list[start], &list[pivot_i]);
        int pivot = list[pivot_i];
        int a = start+1;
        int b = end;
        while (a < b) {
            while (a <= end && list[a] <= pivot)
                a++;
            while (b >= start && list[b] > pivot)
                b--;
            if (a < b)
                swap(&list[a], &list[b]);
        }
        swap(&list[start], &list[b]);
        quicksort(list, start, b - 1);
        quicksort(list, b + 1, end);
    }
}

int compare_ints(const void* a, const void* b) {
    const int *arg1 = a;
    const int *arg2 = b;
    return *arg1 - *arg2;
}

int main() {
    // Read number of test cases
    int cases;
    scanf("%d", &cases);
    int c;
    for (c = 0; c < cases; c++) {
        // Read number of digits
        int digits;
        scanf("%d", &digits);
        // Read digits into array
        //fprintf(stderr, "Reading %d digits\n", digits);
        int* number = calloc(digits, sizeof(int));
        int d;
        for (d = 0; d < digits; d++) {
            scanf("%d", &number[d]);
        }
        //fprintf(stderr, "Looking for swap value\n");
        // Read digits from the end until we find a digit smaller than its
        // predecessor
        int i, swap_i = -1;
        for (i = digits - 2; i > -1; i--) {
            if (number[i] < number[i+1]) {
                swap_i = i;
                break;
            }
        }
        if (swap_i == -1) {
            // Impossible
            printf("-1\n");
            continue;
        }
        //fprintf(stderr, "Swap on digit %d: %d\n", swap_i, number[swap_i]);
        // Sort the digits after the swap
        //fprintf(stderr, "Sorting\n");
        //quicksort(number, swap_i + 1, digits - 1);
        qsort(&number[swap_i + 1], digits - (swap_i + 1), sizeof(int), compare_ints);
        //fprintf(stderr, "sorted: ");
        //for (i = 0; i < digits; i++) {
        //    fprintf(stderr, "%d", number[i]);
        //}
        //fprintf(stderr, "\n");
        //fprintf(stderr, "Looking for digit to swap\n");
        // Loop toward the end to find the first digit greater than the swap
        for (i = swap_i + 1; i < digits; i++) {
            if (number[i] > number[swap_i]) {
                swap(&number[i], &number[swap_i]);
                break;
            }
        }
        //fprintf(stderr, "Printing\n");
        // Print the number
        for (i = 0; i < digits; i++) {
            printf("%d", number[i]);
        }
        printf("\n");
    }
    return 0;
}
