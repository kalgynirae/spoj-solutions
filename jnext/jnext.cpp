#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int main() {
    int cases;
    cin >> cases;
    for (int c = 0; c < cases; c++) {
        int digits;
        cin >> digits;
        vector<char> number;
        number.push_back('0');
        char d;
        for (int i = 0; i < digits; i++) {
            cin >> d;
            number.push_back(d);
        }
        next_permutation(number.begin(), number.end());
        if (number[0] == '0') {
            for (int i = 0; i < digits; i++) {
                cout << number[i+1];
            }
        }
        else {
            cout << "-1";
        }
        cout << endl;
    }
}
