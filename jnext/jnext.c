#include <stdio.h>
#include <stdlib.h>

void swap(int* x, int* y) {
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

void heapsort(int arr[], unsigned int N) {
    unsigned int n = N, i = n/2, parent, child;
    int t;
    for (;;) { /* Loops until arr is sorted */
        if (i > 0) { /* First stage - Sorting the heap */
            i--;           /* Save its index to i */
            t = arr[i];    /* Save parent value to t */
        } else {     /* Second stage - Extracting elements in-place */
            n--;           /* Make the new heap smaller */
            if (n == 0) return; /* When the heap is empty, we are done */
            t = arr[n];    /* Save last value (it will be overwritten) */
            arr[n] = arr[0]; /* Save largest value at the end of arr */
        }
        parent = i; /* We will start pushing down t from parent */
        child = i*2 + 1; /* parent's left child */
        /* Sift operation - pushing the value of t down the heap */
        while (child < n) {
            if (child + 1 < n  &&  arr[child + 1] > arr[child]) {
                child++; /* Choose the largest child */
            }
            if (arr[child] > t) { /* If any child is bigger than the parent */
                arr[parent] = arr[child]; /* Move the largest child up */
                parent = child; /* Move parent pointer to this child */
                //child = parent*2-1; /* Find the next child */
                child = parent*2+1; /* the previous line is wrong*/
            } else {
                break; /* t's place is found */
            }
        }
        arr[parent] = t; /* We save t in the heap */
    }
}

int compare_ints(const void* a, const void* b) {
    const int *arg1 = a;
    const int *arg2 = b;
    return *arg1 - *arg2;
}

int main() {
    // Read number of test cases
    int cases;
    scanf("%d", &cases);
    int c;
    for (c = 0; c < cases; c++) {
        // Read number of digits
        int digits;
        scanf("%d", &digits);
        // Read digits into array
        int* number = calloc(digits, sizeof(int));
        int d;
        for (d = 0; d < digits; d++) {
            scanf("%d", &number[d]);
        }
        // Read digits from the end until we find a digit smaller than its
        // predecessor
        int i, swap_i = -1;
        for (i = digits - 2; i > -1; i--) {
            if (number[i] < number[i+1]) {
                swap_i = i;
                break;
            }
        }
        if (swap_i == -1) {
            // Impossible
            printf("-1\n");
            continue;
        }
        // Sort the digits after the swap
        //qsort(&number[swap_i + 1], digits - (swap_i + 1), sizeof(int), compare_ints);
        heapsort(&number[swap_i + 1], digits - (swap_i + 1));
        // Loop toward the end to find the first digit greater than the swap
        for (i = swap_i + 1; i < digits; i++) {
            if (number[i] > number[swap_i]) {
                swap(&number[i], &number[swap_i]);
                break;
            }
        }
        // Print the number
        for (i = 0; i < digits; i++) {
            printf("%d", number[i]);
        }
        printf("\n");
    }
    return 0;
}
